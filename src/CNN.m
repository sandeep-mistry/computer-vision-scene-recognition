clc;
clear all;
close all;
Class={};
setDir  = fullfile('trainDir')
imagedstore = imageDatastore(setDir,'IncludeSubfolders',true,'LabelSource', 'foldernames')
table = countEachLabel(imagedstore)

% Load pre-trained network (uncomment only one)
network = alexnet();
%network = resnet50();
%network = inceptionresnetv2();

% Split image data into training and test sets
[testSet, trainingSet] = splitEachLabel(imagedstore, 0.1, 'randomize');
% Pre-process images to network dimension requirements
imageSize = network.Layers(1).InputSize;
ResizedTrainingSet = augmentedImageDatastore(imageSize, trainingSet, 'ColorPreprocessing', 'gray2rgb');
ResizedTestSet = augmentedImageDatastore(imageSize, testSet, 'ColorPreprocessing', 'gray2rgb');

% Get network weights for second convolution layer
w_1 = network.Layers(2).Weights;
% Scale and resize the weights for visualisation
w_1 = mat2gray(w_1);
w_1 = imresize(w_1,5);
% Display montage of network weights
figure
montage(w_1)
title('First convolution layer weights')

% Extract training features from layer before classification layer
% Uncomment only one featureLayer corresponding to chosen pre-trained network

featureLayer = 'fc8'; % alexnet
%featureLayer = 'fc1000'; % resnet50
%featureLayer = 'predictions'; % inceptionresnetv2

trainingFeatures = activations(network, ResizedTrainingSet, featureLayer, ...
    'MiniBatchSize', 32, 'OutputAs', 'columns');
% Get training labels from the trainingSet
trainingLabels = trainingSet.Labels;
% Train multiclass SVM classifier using Stochastic Gradient Descent
classifier = fitcecoc(trainingFeatures, trainingLabels, ...
    'Learners', 'Linear', 'Coding', 'onevsall', 'ObservationsIn', 'columns');
% Extract test features using CNN
testFeatures = activations(network, ResizedTestSet, featureLayer, ...
    'MiniBatchSize', 32, 'OutputAs', 'columns');

% Pass CNN image features to trained classifier
predictedLabels = predict(classifier, testFeatures, 'ObservationsIn', 'columns');
% Get the known labels
testLabels = testSet.Labels;
% Tabulate the results using a confusion matrix.
confusionMatrix = confusionmat(testLabels, predictedLabels);
% Convert confusion matrix into percentage form
confusionMatrix = bsxfun(@rdivide,confusionMatrix,sum(confusionMatrix,2))
% Display the mean accuracy
mean(diag(confusionMatrix))

% Apply classifier to new test images
addpath('testPath');
%a=[0:1313 1315:2937 2939:2961 2963:2987]; 
a=[0:1]; 
for z=1:length(a)
    newImage=imread(sprintf('%d.jpg',a(z)));
    % Resize images
    dataStore = augmentedImageDatastore(imageSize, newImage, 'ColorPreprocessing', 'gray2rgb');
    % Extract image features using CNN
    imageFeatures = activations(network, dataStore, featureLayer, 'OutputAs', 'columns');
    % Predict image class using the classifier
    label = predict(classifier, imageFeatures, 'ObservationsIn', 'columns')
end

