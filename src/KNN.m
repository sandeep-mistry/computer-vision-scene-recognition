clc;
clear all;
close all;

Data=zeros(1500,256);
Data1=zeros(2985,256);
no=0;
no1=0;

Class={};
str={'bedroom' 'Coast' 'Forest' 'Highway' 'industrial' 'Insidecity' 'kitchen' 'livingroom' 'Mountain' 'Office' 'OpenCountry' 'store' 'Street' 'Suburb' 'TallBuilding'};
Index   = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,13,14,15];
chosen  = str(Index);

for j = 1:15
    addpath(sprintf('training_data_path',chosen{j}));
    for i=0:99
        no=no+1;
        Img1=imread(sprintf('%d.jpg',i));
        vector = preprocess(Img1);
        Data(no,:) = Data(no,:)+vector;
    end
end

addpath('testing_data_path');
a=[0:1313 1315:2937 2939:2961 2963:2987];

for z=1:length(a)
    no1=no1+1;
    Test_Img=imread(sprintf('%d.jpg',a(z)));
    Img = preprocess(Test_Img);
    Data1(no1,:)=Data1(no1,:)+Img;
end

Idx = knnsearch(Data,Data1,'K',15);
% for each row in #idx do this 

for i = 1:2985
    imageindex = i-1;
    class = (mode(ceil(Idx(i,:) / 100)));
    fprintf('Image %d is',imageindex)
    c=str(class)
    Class=[Class,c];
    %fprintf('image %d', imageindex,' is %s', class)
end

Class=transpose(Class);
