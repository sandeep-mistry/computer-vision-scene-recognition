clc;
clear all;
close all;

trainDir  = fullfile('trainDir');
trainingSet = imageDatastore(trainDir,'IncludeSubfolders',true,'LabelSource','foldernames');

bag_of_words =  bagOfFeatures(trainingSet);

categoryClassifier = trainImageCategoryClassifier(trainingSet,bag_of_words);

testDir  = fullfile('testDir');
testSet = imageDatastore(testDir);
[n p]=size(testSet.Files);

fileID = fopen('classes.txt','w');
for i = 1:n
    index=string(testSet.Files(i));
    [path,name,ext] = fileparts(index);
    test=readimage(testSet,i);
    [labelIdx, score] = predict(categoryClassifier,test);
    class=string(categoryClassifier.Labels(labelIdx));
    fprintf(fileID,'%s.jpg %s\r\n',name,class);
end
fclose(fileID);
