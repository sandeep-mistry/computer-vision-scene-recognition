#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from skimage.util.shape import view_as_windows
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans
import math
import os
from skimage.transform import resize
from sklearn.svm import SVC

# variables place here for configurability
train_root_folder = 'trainforlabeledtest'
test_root_folder = 'labeledtest'

folders = ('bedroom', 'Coast', 'Forest', 'Highway', 'industrial', 'Insidecity', 'kitchen', 'livingroom',
           'Mountain', 'Office', 'OpenCountry', 'store', 'Street', 'Suburb', 'TallBuilding')
num_classes = np.array(folders).shape[0]  # assumes each folder is a class

# we encounter a bug if we only have one directory to access, quick work around is to add an empty directory next to it
test_images = ('bedroom', 'Coast', 'Forest', 'Highway', 'industrial', 'Insidecity', 'kitchen', 'livingroom',
           'Mountain', 'Office', 'OpenCountry', 'store', 'Street', 'Suburb', 'TallBuilding')

num_clusters = 500

resize_scale = 256
window_shape_length = 8
sample_seperation = 4
images_per_class = 90

num_test_images = 150
num_test_classes = 15

batch_size = 1000

test_labeled_data = True

num_train_images = images_per_class * num_classes


def load_images_from_folder(folder):
    images = []
    for file_name in os.listdir(folder):
        if file_name.endswith(".jpg"):
            img = mpimg.imread(os.path.join(folder, file_name))
            if img is not None:
                img = resize(img, (resize_scale, resize_scale), anti_aliasing=True)
                images.append(img)

    return images


def get_normalised_patches(images, resize_scale, sample_seperation, window_shape):
    normalised_patches = np.empty((0, window_shape[0]*window_shape[1]), int)

    for i in range(images.shape[0]):
        # patches = view_as_windows(all_images[i - 1], window_shape)
        patches = view_as_windows(images[i], window_shape)  # removed - 1
        feature_patches = patches[0][::sample_seperation]
        for i in range(1, math.floor(patches.shape[0] / sample_seperation) + 1):
            feature_patches = np.append(feature_patches, patches[i * sample_seperation][::sample_seperation], axis=0)

        flat_patches = []
        for i in range(feature_patches.shape[0]):
            patch = feature_patches[i].flatten()
            patch = patch - np.mean(patch)
            patch = patch / np.linalg.norm(patch)
            flat_patches.append(patch)

        flat_patches = np.array(flat_patches)
        flat_patches = np.nan_to_num(flat_patches)
        normalised_patches = np.vstack([normalised_patches, flat_patches])

    return normalised_patches


def get_vocab_histograms(indexes, patches, num_images, num_clusters):
    vocab_histograms = []
    patches_per_image = math.floor(patches.shape[0] / num_images)

    for image in range(num_images):
        detected_indexes = indexes[image * patches_per_image:(image + 1) * patches_per_image]
        histogram = np.zeros(num_clusters)
        for i in detected_indexes:
            histogram[i] = histogram[i] + 1
        vocab_histograms.append(histogram)

    return np.array(vocab_histograms)


folders = [os.path.join(train_root_folder, x) for x in folders]
all_images = [img for folder in folders for img in load_images_from_folder(folder)]
all_images = np.array(all_images)
window_shape = (window_shape_length, window_shape_length)
normalised_train_patches = get_normalised_patches(all_images, resize_scale, sample_seperation, window_shape)

print("Performing Clustering...")
kmeans = MiniBatchKMeans(n_clusters=num_clusters, batch_size=batch_size)
kmeans.fit(normalised_train_patches)

# for all patches, get their indices
patch_labels = kmeans.fit_predict(normalised_train_patches)
train_vocab_histograms = get_vocab_histograms(patch_labels, normalised_train_patches, num_train_images, num_clusters)

# histograms for each image obtained, y is our target for train_vocab_histograms above
y = []
for c in range(num_classes):
    y.extend(np.full(images_per_class, c))
y = np.array(y)

clf = SVC(gamma='auto')
clf.fit(train_vocab_histograms, y)

# model trained, read a test image, obtain its histogram, and predict it using clf
folders = [os.path.join(test_root_folder, x) for x in test_images]
test_images = [img for folder in folders for img in load_images_from_folder(folder)]
test_images = np.array(test_images)

# to store normalised patches of the test images
normalised_test_patches = get_normalised_patches(test_images, resize_scale, sample_seperation, window_shape)

# obtain the vocab for our test images
patch_labels = kmeans.fit_predict(normalised_test_patches)

test_vocab_histograms = get_vocab_histograms(patch_labels, normalised_test_patches, num_test_images, num_clusters)
prediction = clf.predict(test_vocab_histograms)

for i in prediction:
    print(i)

if test_labeled_data:
    target = []
    for t in range(num_test_classes):
        target.extend(np.full(math.floor(num_test_images/num_test_classes), t))
    target = np.array(target)

    score = 0
    for i in range(prediction.shape[0]):
        if prediction[i] == target[i]:
            score = score + 1

    print("approximate accuracy is: ")
    print(score/num_test_images)
    print(target)




