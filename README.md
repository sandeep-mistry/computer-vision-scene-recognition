# Computer Vision Scene Recognition

# Introduction

We implement various classifiers to label images of different scenes correctly.

# Requirements

Python 3.6.8 was used for this project.

```
pip install -r requirements.txt
```

# Usage
Download training_data.zip from [here](https://drive.google.com/file/d/1UGhBZvPJ4MYHeurMi92lV0IeQRka8-Jy/view?usp=sharing) and testing_data.zip from [here](https://drive.google.com/file/d/1VRwit_wIeEa5fHZnKvoVfzsLfjsCWMjq/view?usp=sharing).

Set training data and test data directories in chosen script.

Run .m scripts in MATLAB

For python scripts:
```
python [model_name].py
```



